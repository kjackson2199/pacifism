﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    public float speed = 5;
    Rigidbody body;
    public static PlayerController player;
    public float bulletSpeed = 50;
    public float roundsPerSecond = 20;
    public GameObject bulletPrefab;

    GameObject[] bullets;
    public int maxRounds = 60;

    public bool bPassive = true;
    bool bReloading = false;

    private void Awake()
    {
        if (player == null) player = this;
    }

    void Start()
    {
        body = GetComponent<Rigidbody>();
        bullets = new GameObject[maxRounds];
        for(int i = 0; i < maxRounds; i++)
        {
            GameObject b = Instantiate(bulletPrefab);
            b.SetActive(false);
            bullets[i] = b;
        }
    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Vector3 dir = new Vector3(x, 0, z);
        if(dir.sqrMagnitude > 1)
        {
            dir.Normalize();
        }

        transform.forward = dir;
        body.velocity = dir * speed;

        if(Input.GetKey(KeyCode.Space) && !bPassive && !bReloading)
        {
            StartCoroutine(ShootBullet());
            Debug.Log("Shooting");
        }
    }

    public static void Die()
    {
        player.StartCoroutine(player.EndGameCoroutine());
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<EnemyController>() || collision.gameObject.GetComponent<ShootingEnemyController>() || collision.gameObject.GetComponent<ExplodingEnemyController>())
        {
            Die();
        }
    }

    IEnumerator ShootBullet()
    {
        bReloading = true;
        for (int i = 0; i < bullets.Length; i++)
        {
            if (!bullets[i].activeSelf)
            {
                bullets[i].SetActive(true);
                bullets[i].GetComponent<PlayerBulletController>().Shoot(transform.forward, transform.position);
                break;
            }
        }
        for (int i = 0; i < bullets.Length; i++)
        {
            if (!bullets[i].activeSelf)
            {
                bullets[i].SetActive(true);
                bullets[i].GetComponent<PlayerBulletController>().Shoot(transform.forward * -1, transform.position);
                break;
            }
        }
        AudioManager.Play("PlayerBlaster");
        yield return new WaitForSeconds(1 / roundsPerSecond);
        bReloading = false;
    }

    IEnumerator EndGameCoroutine()
    {
        AudioManager.Play("PlayerHit");
        yield return new WaitForSeconds(.5f);
        PacifismGameManager.EndGame();
    }
}
