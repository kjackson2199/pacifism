﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public static EnemySpawner instance;

    public GameObject enemyPrefab;
    public int maxEnemies = 100;
    public float interval = 2;
    public float range = 10;
    public int maxRandSpawnCount = 5;

    AudioManager audioManager;
    GameObject[] enemies;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    private void Start()
    {
        enemies = new GameObject[maxEnemies];
        for(int i =0;i<maxEnemies;i++)
        {
            GameObject g = Instantiate(enemyPrefab);
            g.SetActive(false);
            enemies[i] = g;
        }
        StartCoroutine(SpawnCoroutine());
    }
    public GameObject Spawn()
    {
        for(int i = 0; i < maxEnemies; i++)
        {
            if(!enemies[i].activeSelf)
            {
                return enemies[i];
            }
        }
        return null;
    }

    IEnumerator SpawnCoroutine()
    {
        yield return new WaitForSeconds(5f);
        while(enabled)
        {
            AudioManager.Play("EnemySpawn");
            int randSpawnCount = Random.Range(1, maxRandSpawnCount);
            Vector3[] spawnPos = new Vector3[maxRandSpawnCount];

            for(int i = 0; i < randSpawnCount; i++)
            {
                Vector3 spawnLocation = new Vector3(Random.Range(-range, range), 0, Random.Range(-range, range));
                ParticleEffectsManager.Play("EnemySpawnWarning", spawnLocation);
                spawnPos[i] = spawnLocation;
            }
            yield return new WaitForSeconds(2);
            for(int i = 0; i < randSpawnCount; i++)
            {
                GameObject g = Spawn();
                g.transform.position = spawnPos[i];
                g.SetActive(true);
            }
            yield return new WaitForSeconds(interval);
        }
    }
}
