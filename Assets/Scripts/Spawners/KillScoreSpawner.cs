﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillScoreSpawner : MonoBehaviour
{
    public static KillScoreSpawner instance;
    public GameObject scoreObject;
    public int maxScoreObjects = 100;

    GameObject[] scoreObjects;
    float displayTime = 3;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
        scoreObjects = new GameObject[maxScoreObjects];
        for(int s = 0; s < maxScoreObjects; s++)
        {
            GameObject sObject = Instantiate(scoreObject);
            sObject.SetActive(false);
            scoreObjects[s] = sObject;
        }
    }

    public static void DisplayKillScoreAtLocation(int numPoints, Vector3 pos)
    {
        Debug.Log("Score");
        for (int i = 0; i < instance.maxScoreObjects; i++)
        {
            if (!instance.scoreObjects[i].activeSelf)
            {
                GameObject killScoreDisplay = instance.scoreObjects[i];
                killScoreDisplay.SetActive(true);
                killScoreDisplay.transform.position = new Vector3(pos.x, 0, pos.z);

                Text killScoreText = killScoreDisplay.GetComponentInChildren<Text>();
                killScoreText.enabled = true;
                killScoreText.text = numPoints.ToString();

                instance.StartCoroutine(instance.KillScoreDiplayTimeout(killScoreDisplay));
                return;
            }
        }
    }
    
    IEnumerator KillScoreDiplayTimeout(GameObject killScoreDisplay)
    {
        yield return new WaitForSeconds(displayTime);
        killScoreDisplay.SetActive(false);
        Debug.Log("Timeout");
    }
}
