﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierSpawner : MonoBehaviour
{
    public static MultiplierSpawner instance;

    public int maxMultipliers = 100;
    public GameObject multiplierPrefab;
    public int maxMultipliersOnSpawn = 5;
    public float spawnRange = 1;

    GameObject[] multipliers;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
        multipliers = new GameObject[maxMultipliers];
        for(int i = 0; i < maxMultipliers; i++)
        {
            GameObject m = Instantiate(multiplierPrefab);
            m.SetActive(false);
            multipliers[i] = m;
        }
    }

    public static void SpawnMultipliers(Bounds bounds)
    {
        int numMultipliersToSpawn = Mathf.RoundToInt(Random.Range(0, instance.maxMultipliersOnSpawn));
        for(int i = 0; i < numMultipliersToSpawn; i++)
        {
            float x = Random.Range(-instance.spawnRange, instance.spawnRange) + bounds.center.x;
            float z = Random.Range(-instance.spawnRange, instance.spawnRange) + bounds.center.z;

            Vector3 multiplierSpawnPos = new Vector3(x, 0, z);
            for(int m = 0; m < instance.maxMultipliers; m++)
            {
                if(!instance.multipliers[m].activeSelf)
                {
                    instance.multipliers[m].transform.position = multiplierSpawnPos;
                    instance.multipliers[m].SetActive(true);
                    instance.multipliers[m].GetComponent<MultiplierController>().StartTimeoutTimer();
                    break;
                }
            }
        }
    }
}
