﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSpawner : MonoBehaviour
{
    public static ExplosionSpawner instance;

    public GameObject explosionPrefab;
    static int maxExplosions = 20;

    AudioManager audioManager;
    GameObject[] explosions;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
        explosions = new GameObject[maxExplosions];
        for(int i = 0; i < maxExplosions; i++)
        {
            GameObject e = Instantiate(explosionPrefab);
            e.SetActive(false);
            explosions[i] = e;
        }
    }

    public static GameObject SpawnExplosion(Vector3 explosionPos)
    {
        for(int i = 0; i < maxExplosions; i++)
        {
            if (!instance.explosions[i].activeSelf)
            {
                ParticleEffectsManager.Play("GateExplosion", explosionPos);
                instance.explosions[i].transform.position = explosionPos;
                return instance.explosions[i];
            }
        }
        return null;
    }
}
