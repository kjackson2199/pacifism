﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemyController : MonoBehaviour
{
    public float speed = 2;
    public float attackRange = 1;
    public float attackChargeTime = 3;
    public float attackDuration = 5;
    public int pointsOnKill = 100;
    public float roundsPerSecond = 10;
    public int maxRounds = 30;

    public GameObject bulletPrefab;

    Rigidbody body;
    Animator anim;
    bool bAttacking = false;
    GameObject[] bullets;

    private void Start()
    {
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        bullets = new GameObject[maxRounds];

        for(int i = 0; i < maxRounds; i++)
        {
            GameObject b = Instantiate(bulletPrefab);
            b.transform.position = transform.position;
            b.SetActive(false);
            bullets[i] = b;
        }
    }

    public void Update()
    {
        Vector3 dir = PlayerController.player.transform.position - transform.position;
        if (dir.sqrMagnitude > 1) dir.Normalize();

        transform.forward = dir;
        if (!bAttacking)
        {
            body.velocity = dir * speed;
        }
        else if(bAttacking)
        {
            return;
        }

        if(Vector3.Distance(PlayerController.player.transform.position, transform.position) < attackRange)
        {
            bAttacking = true;
            StartCoroutine(Attack());
        }
    }

    void FireBullet()
    {
        for (int i = 0; i < bullets.Length; i++)
        {
            if(!bullets[i].activeSelf)
            {
                bullets[i].SetActive(true);
                bullets[i].GetComponent<ShootingEnemyBulletController>().Shoot(transform.forward, transform.position);
                break;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<PlayerBulletController>())
        {
            Die();
            PacifismGameManager.UpdateScore(pointsOnKill);
            KillScoreSpawner.DisplayKillScoreAtLocation(pointsOnKill, transform.position);
        }
    }


    IEnumerator Attack()
    {
        Debug.Log("Start Attack");
        ChargeAttack();
        yield return new WaitForSeconds(attackChargeTime);
        StartCoroutine(Shoot());
        yield return new WaitForSeconds(attackDuration);
        bAttacking = false;
        Debug.Log("End Attack");
    }

    void ChargeAttack()
    {
        Debug.Log("ChargeAttack");
        anim.SetTrigger("Charge");
        AudioManager.Play("WeaponChargeSound");
    }

    IEnumerator Shoot()
    {
        Debug.Log("Firing My Lazer");
        float rps = 1 / roundsPerSecond;
        for (float t= 0; t < attackDuration; t += Time.deltaTime + rps)
        {
            AudioManager.Play("EnemyBlaster");
            FireBullet();
            yield return new WaitForSeconds(rps);
        }
    }

    void Die()
    {
        gameObject.SetActive(false);
        MultiplierSpawner.SpawnMultipliers(GetComponent<Collider>().bounds);
    }
}
