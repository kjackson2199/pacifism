﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingEnemyController : MonoBehaviour
{
    public float speed = 5;
    public float activationRange = 2;
    public float timerCountdown = 5;
    public int pointsOnKill = 100;
    public float explosionLifetime = .3f;
    public GameObject explosionObject;

    Rigidbody body;
    bool bCountingDown = false;

    private void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Vector3 dir = PlayerController.player.transform.position - transform.position;
        if (dir.sqrMagnitude > 1) dir.Normalize();

        transform.forward = dir;
        body.velocity = dir * speed;

        if (Vector3.Distance(PlayerController.player.transform.position, transform.position) < activationRange && !bCountingDown)
        {
            StartCoroutine(BombCountdown());
        }
    }

    private void OnEnable()
    {
        if (explosionObject.activeSelf) explosionObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<PlayerBulletController>())
        {
            Die();
            PacifismGameManager.UpdateScore(pointsOnKill);
            KillScoreSpawner.DisplayKillScoreAtLocation(pointsOnKill, transform.position);
        }
    }

    IEnumerator BombCountdown()
    {
        bCountingDown = true;
        StartCoroutine(BombSFX());
        for (float timer = 0; timer < timerCountdown; timer += Time.deltaTime)
        {
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(Explode());
    }

    IEnumerator BombSFX()
    {
        while(enabled)
        {
            AudioManager.Play("BombTickSound");
            yield return new WaitForSeconds(.5f);
        }
    }

    IEnumerator Explode()
    {
        if (explosionObject == null) yield return null;
        if(!explosionObject.activeSelf)
        {
            AudioManager.Play("Explosion");
            ParticleEffectsManager.Play("GateExplosion", transform.position);
            explosionObject.SetActive(true);
        }
        yield return new WaitForSeconds(explosionLifetime);
        explosionObject.SetActive(false);
        gameObject.SetActive(false);
    }

    void Die()
    {
        gameObject.SetActive(false);
        MultiplierSpawner.SpawnMultipliers(GetComponent<Collider>().bounds);
    }
}
