﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed = 5;
    public int maxPointsOnKill = 100;
    Rigidbody body;
    //static PlayerController player;

    void Start()
    {
        body = GetComponent<Rigidbody>();
        //if(PlayerController.player = null) PlayerController.player = FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        Vector3 dir = PlayerController.player.transform.position - transform.position;
        if (dir.sqrMagnitude > 1)
        {
            dir.Normalize();
        }

        transform.forward = dir;
        body.velocity = dir * speed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<ExplosionController>() || collision.gameObject.GetComponent<PlayerBulletController>())
        {
            Die();
            PacifismGameManager.UpdateScore(maxPointsOnKill);
            KillScoreSpawner.DisplayKillScoreAtLocation(maxPointsOnKill, transform.position);
        }
    }

    void Die()
    {
        gameObject.SetActive(false);
        MultiplierSpawner.SpawnMultipliers(GetComponent<Collider>().bounds);
    }
}
