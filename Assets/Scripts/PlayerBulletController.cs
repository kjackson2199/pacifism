﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletController : MonoBehaviour
{
    public float bulletSpeed = 50;
    public float bulletLifeTime = 1;

    Rigidbody body;
    private void Awake()
    {
        body = GetComponent<Rigidbody>();
    }

    public void Shoot(Vector3 dir, Vector3 pos)
    {
        if (!gameObject.activeSelf) return;
        transform.position = pos;
        body.velocity = bulletSpeed * dir;
        StartCoroutine(lifeTimer());
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<EnemyController>() || collision.gameObject.GetComponent<ShootingEnemyController>() || collision.gameObject.GetComponent<ExplodingEnemyController>())
        {
            gameObject.SetActive(false);
        }
    }

    IEnumerator lifeTimer()
    {
        yield return new WaitForSeconds(bulletLifeTime);
        gameObject.SetActive(false);
    }
}
