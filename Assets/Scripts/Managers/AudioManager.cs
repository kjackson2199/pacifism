﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioClip[] clips;
    public int maxSources = 5;
    AudioSource mainLevelMusicSource;
    public AudioClip mainLevelMusic;

    static AudioSource[] sources;
    static int index = 0;

    private void Awake()
    {
        if (instance == null) instance = this;
        sources = new AudioSource[maxSources];
        for(int i = 0; i < maxSources; i++)
        {
            GameObject g = new GameObject("SFX." + i);
            AudioSource s = g.AddComponent<AudioSource>();
            s.volume = .5f;
            sources[i] = s;
        }

        GameObject mLMSourceObject = new GameObject();
        mainLevelMusicSource = mLMSourceObject.AddComponent<AudioSource>();
    }

    public static void Play(string clipName)
    {
        AudioClip clip = null;
        for(int i = 0; i <instance.clips.Length;i++)
        {
            if (instance.clips[i].name == clipName)
            {
                clip = instance.clips[i];
                break;
            }
        }
        if(clip != null)
        {
            sources[index].clip = clip;
            sources[index].Play();
            index++;
            index %= instance.maxSources;
        }
    }

    public static void PlayLevelMusic()
    {
        if(!instance.mainLevelMusicSource.isPlaying)
        {
            instance.mainLevelMusicSource.clip = instance.mainLevelMusic;
            instance.mainLevelMusicSource.Play();
        }
    }
}
