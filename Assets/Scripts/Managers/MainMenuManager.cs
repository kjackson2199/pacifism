﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public void PlayPacifism()
    {
        SceneManager.LoadScene("Pacifism");
    }

    public void PlayKillmonger()
    {
        SceneManager.LoadScene("Killmonger");
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}
