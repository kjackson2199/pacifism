﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaIntoSequenceManager : MonoBehaviour
{
    public static ArenaIntoSequenceManager instance;
    public float scrollX = 0.1f;
    public float scrollY = 0.1f;

    public float duration = 2f;

    public static bool bReady = false;

    float offsetX = 0;
    float offsetY = 0;

    private void Start()
    {
        if (instance == null) instance = this;
        StartCoroutine(ArenaFloorTextureAnimCoroutine());
    }

    IEnumerator ArenaFloorTextureAnimCoroutine()
    {
        Renderer renderer = GetComponent<Renderer>();
        Color startColor = renderer.material.GetColor("_EmissionColor");

        AudioManager.Play("LevelStart");

        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            offsetX += Time.deltaTime * scrollX;
            offsetY += Time.deltaTime * scrollY;
            renderer.material.mainTextureOffset = new Vector2(offsetX, offsetY);
            yield return new WaitForEndOfFrame();
        }

        for(float timer = 0; timer < 1.5f; timer += Time.deltaTime)
        {
            renderer.material.SetColor("_EmissionColor", Color.Lerp(startColor, new Color(0, 0, 0, 0), timer));
            yield return new WaitForEndOfFrame();
        }

        AudioManager.PlayLevelMusic();
    }
}
