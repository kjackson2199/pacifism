﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffectsManager : MonoBehaviour
{
    public static ParticleEffectsManager instance;
    public ParticleSystem[] particlesPrefabs;
    public int maxParticleFX = 20;

    static ParticleSystem[][] systems;

    static int[] indicies;


    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(gameObject);
        int count = particlesPrefabs.Length;
        systems = new ParticleSystem[count][];
        indicies = new int[count];
        
        for(int i = 0; i < count; i++)
        {
            systems[i] = new ParticleSystem[maxParticleFX];
            for(int j = 0; j < maxParticleFX; j++)
            {
                systems[i][j] = Instantiate(particlesPrefabs[i]);
            }
        }
    }

    public static void Play(string systemName, Vector3 pos)
    {
        for(int i=0; i<instance.particlesPrefabs.Length; i++)
        {
            if(instance.particlesPrefabs[i].name == systemName)
            {
                ParticleSystem[] pool = systems[i];
                int index = indicies[i];
                ParticleSystem system = pool[index];
                system.transform.position = pos;
                system.Play();
                indicies[i] = (index + 1) % instance.maxParticleFX;
                break;
            }
        }
    }
}
