﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PacifismGameManager : MonoBehaviour
{
    public static PacifismGameManager instance;

    public Text highScoreText;
    public Text currentScoreText;
    public Text multiplierText;

    int currentScore = 0;
    int highScore = 0;
    int currentMultiplier = 1;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
        highScore = PlayerPrefs.GetInt("High Score", 0);
        UpdateUI();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    public static void UpdateScore(int pointsToAdd)
    {
        instance.currentScore += pointsToAdd * instance.currentMultiplier;
        instance.UpdateUI();
    }

    public static void UpdateMultiplier(int multiplierValueToAdd)
    {
        instance.currentMultiplier += multiplierValueToAdd;
        instance.UpdateUI();
    }

    void UpdateUI()
    {
        highScoreText.text = highScore.ToString();
        currentScoreText.text = currentScore.ToString();
        multiplierText.text = "x" + currentMultiplier.ToString();
    }

    public static void EndGame()
    {
        if (instance.currentScore > instance.highScore)
        {
            PlayerPrefs.SetInt("High Score", instance.currentScore);
        }

        //EnemySpawner.instance.gameObject.SetActive(false);
        //GateSpawner.instance.gameObject.SetActive(false);

        instance.QuitToMenu();
    }

    void QuitToMenu()
    {
        //EndGame();
        SceneManager.LoadScene("MainMenu");
    }
}
