﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateSpawner : MonoBehaviour
{
    public static GateSpawner instance;

    public GameObject gatePrefab;
    public int maxGates = 10;
    public float interval = 3;
    public float range = 5;

    GameObject[] gates;
    Bounds arenaBounds;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        gates = new GameObject[maxGates];
        for (int i = 0; i < maxGates; i++)
        {
            GameObject g = Instantiate(gatePrefab);
            g.SetActive(false);
            gates[i] = g;
        }
        StartCoroutine(SpawnCoroutine());
    }

    public GameObject Spawn()
    {
        for (int i = 0; i < maxGates; i++)
        {
            if (!gates[i].activeSelf)
            {
                return gates[i];
            }
        }
        return null;
    }

    IEnumerator SpawnCoroutine()
    {
        yield return new WaitForSeconds(5);
        while (enabled)
        {
            AudioManager.Play("GateSpawn");
            Vector3 pos = new Vector3(Random.Range(-range, range), 0, Random.Range(-range, range));
            Vector3 rot = new Vector3(0, Random.Range(0, 360), 0);
            ParticleEffectsManager.Play("GateSpawnWarning", pos);
            yield return new WaitForSeconds(1f);

            GameObject g = Spawn();
            if (g != null)
            {
                g.transform.position = pos;
                g.transform.rotation = Quaternion.Euler(rot);
                g.SetActive(true);
                //AudioManager.Play("spawnGate");
            }
            yield return new WaitForSeconds(interval);
        }
    }
}
