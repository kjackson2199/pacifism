﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierController : MonoBehaviour
{
    public float pullRange = 10;
    public float speed = 2;
    public float timeoutInterval = 5;
    public int multiplierValue = 10;

    public void StartTimeoutTimer()
    {
        StartCoroutine(TimeoutCoroutine());
    }

    private void Update()
    {
        Gravitate();
    }

    public void Gravitate()
    {
        if(Vector3.Distance(PlayerController.player.transform.position, transform.position) < pullRange)
        {
            Vector3 dir = PlayerController.player.transform.position - transform.position;
            transform.Translate(dir.normalized * speed * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<PlayerController>())
        {
            gameObject.SetActive(false);
            PacifismGameManager.UpdateMultiplier(multiplierValue);
            AudioManager.Play("Multiplier");
        }
    }

    IEnumerator TimeoutCoroutine()
    {
        yield return new WaitForSeconds(timeoutInterval);
        gameObject.SetActive(false);
    }
}
