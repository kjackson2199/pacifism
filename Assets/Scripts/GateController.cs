﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : MonoBehaviour
{
    public GameObject[] badGates = new GameObject[2];
    public GameObject goodGate;
    public GameObject explosionPrefab;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<PlayerController>())
        {
            //Debug.Log("Dead");
            AudioManager.Play("EnemyHit");
            PlayerController.Die();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponentInParent<PlayerController>())
        {
            //Debug.Log("Gate");
            AudioManager.Play("Explosion");
            for(int i = 0; i < badGates.Length; i++)
            {
                GameObject explo = ExplosionSpawner.SpawnExplosion(badGates[i].transform.position);
                ExplosionController exploController = explo.GetComponent<ExplosionController>();
                if(explo != null)
                {
                    Rigidbody body = GetComponent<Rigidbody>();
                    explo.SetActive(true);
                    gameObject.SetActive(false);
                    body.velocity = Vector3.zero;
                    exploController.Explode();
                }
            }
        }
    }
}
